let courses = [];

const postCourse = (id, name, description, price, isActive) => {
	courses.push({
		id: id,
		name: name,
		description: description,
		price: price,
		isActive: isActive
	})

	//alert(`You have created ${name}. The price is $${price}`);
	//console.log(`You have created ${name}. The price is $${price}`);

}


const getSingleCourse = (id) => {
	
	let foundId = courses.find((course) => {
		return id === course.id;
	})
	
	if(foundId !== undefined) {
		console.log(`Match for id ${id}: ${foundId.name}-${foundId.description}`);	
	} else {
		console.log(`There are no courses yet with id: ${id}.`)
	}	
}


const deleteCourse = () => {
	courses.pop();
}


postCourse("101", "Zoology", "Life sciences", 100, true);
postCourse("102", "Botany", "Life sciences", 100, true);
postCourse("103", "Chemistry", "Life sciences", 150, true);
postCourse("104", "General Biology", "Life sciences", 100, true);
postCourse("105", "Ecology", "Life sciences", 200, true);
postCourse("110", "Evolution", "Life sciences", 250, true);

console.log(courses);
getSingleCourse("110")
getSingleCourse("201");
console.log(courses.length);
deleteCourse();
console.log(courses.length);
console.log(courses);




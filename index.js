let assets = [
	{
		id: "0001",
		name: "Potion",
		description: "Regenerate Health",
		stock: 10,
		isAvailable: true,
		dateAdded: "Jan 14, 2022"
	},
	{
		id: "0002",
		name: "Mega Potion",
		description: "Regenerate large amount of Health",
		stock: 5,
		isAvailable: true,
		dateAdded: "Jan 14, 2022"
	},
]

console.log(assets);

let jsonSample = `{

	"sampleKey1": "valueA",
	"sampleKey2": "valueB",
	"sampleKey3": 1,
	"sampleKey4": true

}`

console.log(typeof jsonSample);

let jsonConvert = JSON.parse(jsonSample);
console.log(typeof jsonConvert, jsonConvert);

let batches = [
	{
		batch: "Batch 152",
	},
	{
		batch: "Batch 156",
	},
]

let stringify = JSON.stringify(assets);

console.log(typeof stringify);
console.log(stringify);

let data = {
	name: "Katniss",
	age: 20,
	address: {
		city: "Kansas City",
		state: "Kansas"
	}
}

let dataJSON = JSON.stringify(data);
console.log(dataJSON);

let data2 = {
	username: "saitamaOPM",
	password: "onepuuuuunch",
	isAdmin: true
}

let data3 = {
	username: "Light Yagami",
	password: "notKiraDefinitely",
	isAdmin: false
}
let data4 = {
	username: "Llawliett",
	password: "yagamiiskira07",
	isAdmin: false
}

let data2JSON = JSON.stringify(data2);
let data3JSON = JSON.stringify(data3);
let data4JSON = JSON.stringify(data4);
let jsonArray = JSON.stringify(assets);

console.log(data2JSON);
console.log(data3JSON);
console.log(data4JSON);
console.log(jsonArray);

let items = `[
	
	{
		"id": "shop-1",
		"name": "Oreos",
		"stock": 5, 
		"price": 50
	},
	{
		"id": "shop-2",
		"name": "Doritos",
		"stock": 10, 
		"price": 150
	}
]`

let itemsJSArr = JSON.parse(items);
console.log(itemsJSArr);
itemsJSArr.pop();
console.log(itemsJSArr);
items = JSON.stringify(itemsJSArr);
console.log(items);


let courses = `[

	{
		"name": "Math 101",
		"description": "learn the basics of Math",
		"price": 2500
	},
	{
		"name": "Science 101",
		"description": "learn the basics of Science",
		"price": 2500
	}
]`

let coursesJSArr = "";
let parser = (courses) => coursesJSArr = JSON.parse(courses);

parser(courses);
console.log(coursesJSArr);

coursesJSArr.pop();
courses = JSON.stringify(coursesJSArr);
console.log(courses);

parser(courses);

coursesJSArr.push({
	name: "Math 201",
	description: "learn the basics of algebra",
	price: 3000
})
courses = JSON.stringify(coursesJSArr);
console.log(courses);

